#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {

	board = new Board();
	other = (side == BLACK) ? WHITE: BLACK;
    ourSide = side;
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() 
{
	delete board;
	/*for(int i = 0; i < movecount; i++)
	{
		delete todelete[i];
	}*/
	
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
	
    board->doMove(opponentsMove, other);
    int length = this->getMoves(ourSide, board);
    if (length == 0)
    {
        std::cerr << "NULL" << std::endl;
        return NULL;
    }
    int bestScore = -10000000;
    int bestIndex = 0;
    int bestBetaScore = -10000000;
    //int bestBetaIndex = 0;
    int betaLength = 0;
    for(int index = 0; index < length; index ++)
    { 
        Board * testboard = board->copy();
        testboard -> doMove(moves[index], ourSide);
        betaLength = this -> getBetaMoves(other, testboard);
        bestBetaScore = -10000000;
        //bestBetaIndex = 0;
        for(int index2 = 0; index2 < betaLength; index2 ++)
        {
            if (bestBetaScore < betaScores[index2])
            {
                bestBetaScore = betaScores[index2];
                //bestBetaIndex = index2; 
            }
        }
        scores[index] = scores[index] - bestBetaScore;
        if (bestScore < scores[index])
        {
            bestScore = scores[index];
            bestIndex = index; 
        }
        delete testboard;
    } 
    std::cerr << "our move: (" << moves[bestIndex]->x << \
        ", "<< moves[bestIndex]->y << ") " << std::endl;  
    board -> doMove(moves[bestIndex], ourSide);
    return moves[bestIndex];
}
                          
int ExamplePlayer::getScore(Move *ourMove, Side side, Board * brd)
{
    int x = ourMove->x; 
    int y = ourMove->y; 
    Side otherSide = (side == BLACK) ? WHITE : BLACK;
    Board *testboard = brd->copy();
    testboard->doMove(ourMove, side);
    int score = testboard->count(side) - testboard->count(otherSide);

    //edges are good
    if(x % 7 == 0 or y % 7 == 0)
    {
        score += 10;
        //always pick corners  
        if(x % 7 == 0 and y % 7 == 0)
        {
            score += 10;
        }
    }

//if we don't own a corner, don't play next to it
    if((x == 1 and y == 1) or (x == 1 and y == 0) or (x == 0 and y == 1))
    {
        if(!testboard->publicGet(ourSide, 0, 0))
        {
            score -= 20;
        }
    }
    else if((x == 1 and y == 6) or (x == 1 and y == 7) or (x == 0 and y == 6))
    {
        if(!testboard->publicGet(ourSide, 0, 7))
        {
            score -= 20;
        }
    }
    else if((x == 6 and y == 1) or (x == 7 and y == 1) or (x == 6 and y == 0))
    {
        if(!testboard->publicGet(ourSide, 7, 0))
        {
            score -= 20;
        }
    }
    else if((x == 6 and y == 6) or (x == 7 and y == 6) or (x == 6 and y == 7))
    {
        if(!testboard->publicGet(ourSide, 7, 7))
        {
            score -= 20;
        }
    }

//don't play next to an edge unless we have strong position
    if(x == 1 and 1 < y and y < 6)
    {
        if(!(testboard->publicGet(ourSide, 1, y-1) and testboard->publicGet(ourSide, 1, y) and testboard->publicGet(ourSide, 1, y+1)))
        {        
            score -= 5;
        }
    }
    else if(x == 6 and 1 < y and y < 6)
    {
        if(!(testboard->publicGet(ourSide, 6, y-1) and testboard->publicGet(ourSide, 6, y) and testboard->publicGet(ourSide, 6, y+1)))
        {        
            score -= 5;
        }
    }
    else if(1 < x and x < 6 and y == 1)
    {
        if(!(testboard->publicGet(ourSide, x-1, 1) and testboard->publicGet(ourSide, x, 1) and testboard->publicGet(ourSide, x+1, 1)))
        {        
            score -= 5;
        }
    }
    else if(1 < x and x < 6 and y == 6)
    {
        if(!(testboard->publicGet(ourSide, x-1, 6) and testboard->publicGet(ourSide, x, 6) and testboard->publicGet(ourSide, x+1, 6)))
        {        
            score -= 5;
        }
    }
    std::cerr << score << std::endl;

    //delete testboard;
    return score;
}


//returns number of moves
int ExamplePlayer::getMoves(Side side, Board * testboard)
{
    int length = 0;
    for(int i = 0; i < 8; i++) {
        for(int j = 0; j < 8; j++) {
            move = new Move(i, j);
            if(testboard->checkMove(move, side))
            {
                moves[length] = new Move(move->x, move->y);
                scores[length] = this->getScore(moves[length], side, testboard);
                length += 1;
            }   
            delete move; 
        }
    }
    return length;
}
int ExamplePlayer::getBetaMoves(Side side, Board * testboard)
{
    int length = 0;
    for(int i = 0; i < 8; i++) {
        for(int j = 0; j < 8; j++) {
            move = new Move(i, j);
            if(testboard->checkMove(move, side))
            {
                betaMoves[length] = new Move(move->x, move->y);
                betaScores[length] = this->getScore(betaMoves[length], side, testboard);
                length += 1;
            }  
            delete move; 
        }
    }
    return length;
}


    
