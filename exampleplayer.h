#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer {

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
//private:
	Board * board;
	Side other;
	Side ourSide;
	Move* move;
    Move *doMove(Move *opponentsMove, int msLeft);
    int getMoves(Side side, Board * board);
    int getBetaMoves(Side side, Board * testboard);
    int getScore(Move *ourMove, Side side, Board * brd);

	Move* bestmove;
	//Move* todelete[50];
	//int movecount;
	Move * moves[50];
	Move * betaMoves[50];
	int scores[50];
	int betaScores[50];
};

#endif
